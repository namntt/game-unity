﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	public float Speed = 500f, MaxSpeed = 3, JumpPower = 220f;
	public bool IsGrounded = true, FaceRight = true;

	public Rigidbody2D Rb2;
	public Animator Anm;

	public void Flip()
	{
		FaceRight = !FaceRight;
		Vector3 Scale;
		Scale = transform.localScale;
		Scale.x *= -1;
		transform.localScale = Scale;
	}

	// Use this for initialization
	void Start ()
	{
		Rb2 = gameObject.GetComponent<Rigidbody2D>();
		Anm = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		Anm.SetBool("IsGrounded", IsGrounded);
		Anm.SetFloat("Speed", Mathf.Abs(Rb2.velocity.x));

		if (Input.GetKeyDown(KeyCode.Space))
		{
			if (IsGrounded)
			{
				IsGrounded = false;
				Rb2.AddForce(Vector2.up * JumpPower);
			}
		}
	}

	void FixedUpdate()
	{
		var h = Input.GetAxis("Horizontal");
		Rb2.AddForce((Vector2.right) * Speed * h);

		if (Rb2.velocity.x > MaxSpeed)
			Rb2.velocity = new Vector2(MaxSpeed, Rb2.velocity.y);
		if (Rb2.velocity.x < -MaxSpeed)
			Rb2.velocity = new Vector2(-MaxSpeed, Rb2.velocity.y);

		if (h > 0 && !FaceRight) { Flip(); }

		if (h < 0 && FaceRight) { Flip(); }
	}
}
